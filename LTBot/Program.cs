﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using LT;
using LT.Bot;
using LT.Bots;
using LT.Constants;
using LT.Helper;
using LT.Models;

namespace LTBot
{
    class Program
    {
        #region Internal

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Display message. See <see cref="System.String"/>.</param>
        /// <param name="value">Returns inputted value. See <see cref="System.String"/>.</param>
        private static void InputField(string message, out string value)
        {
            Console.Write($"{message}: ");
            value = Console.ReadLine();
        }

        #endregion

        #region Variables

        /// <summary>
        /// Contains object instance of <see cref="System.Boolean"/>.
        /// </summary>
        private static bool BotInitialized { get; set; } = false;

        #endregion

        #endregion

        static void Main(string[] args)
        {
            InputField("Username", out string inputUsername);
            InputField("Password", out string inputPassword);
            var userSettings = new Settings();

            if (!BotInitialized && !string.IsNullOrEmpty(inputUsername) && !string.IsNullOrEmpty(inputPassword))
            {
                // change bot-initialized state
                BotInitialized = true;

                // declare configuration variable
                userSettings = new Settings
                {
                    User =
                    {
                        Username = inputUsername,
                        Password = inputPassword
                    }
                };
            }

            if (BotInitialized)
            {
                // apply user-data

                async void Init()
                {
                    // start bot
                    var bot = new EmergencyBot();
                    await bot.Execute(userSettings);
                }

                Init();
            } // end while-loop
        }
    }
}
