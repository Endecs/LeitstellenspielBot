﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using LT.Constants;
using LT.Helper;
using LT.Models;
using LT.Models.Emergency;
using Newtonsoft.Json;

namespace LT.Bot
{
    public class LTActions
    {
        private readonly HTTP _client;
        private long GenerateId { get; set; }

        public LTActions(HTTP client)
        {
            _client = client;
            GenerateId = new Random().Next(0, int.MaxValue);
        }

        public HtmlDocument Login(User user)
        {
            var loginDoc = _client.DoRequest(Urls.Login);
            var form = loginDoc.GetElementbyId("new_user");
            var post = GetFormValues(form);

            //Set User Info
            if (post.Keys.Contains("user[email]"))
                post["user[email]"] = user.Username;

            if (post.Keys.Contains("user[password]"))
                post["user[password]"] = user.Password;

            var content = new FormUrlEncodedContent(post);

            var postDoc = _client.DoPost(Urls.Login, content);
            return postDoc;
        }

        public Missions Missions()
        {
            GenerateId++;
            _client.DoRequest(Urls.MissionGenerate + GenerateId);
            var mainPage = _client.DoRequest(Urls.MainPage);
            return GetMissions(mainPage);
        }

        //TODO: Make it public and mission info as result
        private Dictionary<string, string> MissionInfo(int id)
        {
            var missionInfoDoc = _client.DoRequest(Urls.Mission + id);
            /*var vehicleTable = missionInfoDoc.GetElementbyId("vehicle_show_table_body_all");

            var verhicles = vehicleTable.Descendants("tr");*/
            var form = missionInfoDoc.GetElementbyId("mission-form");

            //TODO: Gets only the first Vehicle
            var post = GetFormValues(form);

            return post;
        }

        //TODO: When changing MissionInfo adjust this also
        public StartMissionState StartMission(int id)
        {
            var missionInfo = MissionInfo(id);

            if (!missionInfo.ContainsKey("vehicle_ids[]") || string.IsNullOrEmpty(missionInfo["vehicle_ids[]"]))
                return StartMissionState.CurrentlyNoAvailableVehicle;

            var content = new FormUrlEncodedContent(missionInfo);

            //TODO: format url
            var missionResult = _client.DoPost(Urls.Mission + id + "/alarm", content);
            var missions = Missions();

            var mission = missions.OwnMissions.FirstOrDefault(m => m.id == id);
            if (mission == null)
                return StartMissionState.Failed;

            //mtid 76 = Küchenbrand

            if (mission.vehicle_state != MissionState.Idle)
                return StartMissionState.Started;

            return StartMissionState.Failed;
        }

        private Missions GetMissions(HtmlDocument doc)
        {
            var script = doc.DocumentNode
                .Descendants("script")
                .FirstOrDefault(s => s.InnerText.Contains("missionMarkerAdd"));

            var missionMarkers = StringOperation.BetweenCollection(script.InnerText, "missionMarkerAdd", ";");
            var missions = new List<Mission>();

            foreach (Match missionMarker in missionMarkers)
            {
                var tmp = missionMarker.Value.Replace("missionMarkerAdd( ", "").Replace(");", "");
                var mission = JsonConvert.DeserializeObject<Mission>(tmp);
                missions.Add(mission);
            }

            var retn = new Missions();
            retn.OwnMissions = missions.Where(m => m.alliance_id == null).ToList();
            retn.AllianceMissions = missions.Where(m => m.alliance_id != null).ToList();

            return retn;
        }

        private Dictionary<string, string> GetFormValues(HtmlNode form)
        {
            var inputs = form.Descendants("input");

            var values = new Dictionary<string, string>();

            foreach (var input in inputs)
            {
                var name = input.GetAttributeValue("name", String.Empty);
                var value = input.GetAttributeValue("value", String.Empty);

                if (values.Keys.Contains(name))
                    continue;
                
                values.Add(name, value);
            }

            return values;
        }
    }
}
