﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LT.Models.Emergency
{
    public class Mission
    {
        public int sw_start_in { get; set; }
        public bool sw { get; set; }
        public int tv { get; set; }
        public int mtid { get; set; }
        public bool kt { get; set; }
        public object alliance_id { get; set; }
        public int prisoners_count { get; set; }
        public int patients_count { get; set; }
        public int user_id { get; set; }
        public string address { get; set; }
        public MissionState vehicle_state { get; set; }
        public object missing_text { get; set; }
        public object missing_text_short { get; set; }
        public int id { get; set; }
        public float live_current_value { get; set; }
        public string finish_url { get; set; }
        public int date_end { get; set; }
        public int date_now { get; set; }
        public double longitude { get; set; }
        public double latitude { get; set; }
        public object tlng { get; set; }
        public object tlat { get; set; }
        public string icon { get; set; }
        public string caption { get; set; }
        public string captionOld { get; set; }
    }

    public enum MissionState
    {
        Idle = 0,
        Driving = 1,
        Solving = 2
    }

    public enum StartMissionState
    {
        Failed = 0,
        Started = 1,
        CurrentlyNoAvailableVehicle = 2,
    }
}
