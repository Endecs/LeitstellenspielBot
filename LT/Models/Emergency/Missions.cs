﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace LT.Models.Emergency
{
    public class Missions
    {
        public List<Mission> OwnMissions { get; set; }
        public List<Mission> AllianceMissions { get; set; }
    }
}
