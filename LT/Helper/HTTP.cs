﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using LT.Constants;

namespace LT.Helper
{
    public class HTTP
    {
        private HttpClient Client { get; set; }

        public HTTP(HttpClient client)
        {
            Client = client;
        }

        public HTTP()
        {
            Client = new HttpClient();
        }

        public HtmlDocument DoRequest(string url)
        {
            var documentString = Client.GetStringAsync(url).Result;

            return CreateDocument(documentString);
        }

        public HtmlDocument DoPost(string url, HttpContent content)
        {
            var response = Client.PostAsync(url, content).Result;
            var documentString = response.Content.ReadAsStringAsync().Result;
            
            return CreateDocument(documentString);
        }

        private HtmlDocument CreateDocument(string doc)
        {
            var document = new HtmlDocument();
            document.LoadHtml(doc);
            HtmlNode.ElementsFlags.Remove("form");

            return document;
        }

    }
}
