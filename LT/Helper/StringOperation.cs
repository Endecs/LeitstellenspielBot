﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace LT.Helper
{
    public static class StringOperation
    {
        public static string Between(string sInput, string sStart, string sEnd, int aMatch = 1, string Regular = "(.*?)")
        {
            Regex regex = new Regex(sStart + Regular + sEnd);
            Match match = regex.Match(sInput);

            if (match.Success)
                return match.Groups[aMatch].Value;

            return string.Empty;
        }

        public static MatchCollection BetweenCollection(string sInput, string sStart, string sEnd, string Regular = "(.*?)")
        {
            Regex regex = new Regex(sStart + Regular + sEnd);
            var match = regex.Matches(sInput);

            return match;
        }

        public static int CountWords(string aSource, string aSearch)
        {
            return Regex.Matches(aSource, aSearch).Count;
        }
    }
}
